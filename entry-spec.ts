import { Entry } from './entry';
import { EntryTestHelper } from '../../../../spec-helpers/spec-helpers';
import Comment from '../comment/comment';
import Note from '../note/note';

describe('Entry model', () => {

  let SubEntry = class extends Entry {
    type = 'subEntry';
    name = 'sub entry';
    isEstimateable() { return false; }
  };

  let entry: Entry,
      comment: Comment;

  describe('Property creation', () => {

    it('should initialize deleted to to false', () => {
      entry = new SubEntry('2016-12-01', EntryTestHelper.getEntryData());
      expect(entry.deleted).toEqual(false);
    });

    it('should use shortName for the label', () => {
      expect(entry.data.codeName).toEqual('Hours Worked');
    });

    it('should create earning allocations if present', () => {
      entry = new SubEntry('2016-12-01', {laborAllocations: [1]});
      expect(entry.data.earningAllocations).toEqual([1]);
    });

    it('should set earningAllocations to empty array if not present', () => {
      entry = new SubEntry('2016-12-01', {});
      expect(entry.data.earningAllocations).toEqual([]);
    });

    it('should set commentIndicator to true if noteIndicator is present', () => {
      entry = new SubEntry('2016-12-01', {comments: [
        {
          commentCode: {
            codeValue: 'Mobile'
          },
          appliesToCode: 'Start Time'
        }
      ]});
      entry.addComment(comment);
      expect(entry.data.commentIndicator).toBe(true);
    });

    it('should not set commentIndicator if noteIndicator is not present', () => {
      entry = new SubEntry('2016-12-01', {});
      expect(entry.data.commentIndicator).toBe(false);
    });

    it('should initialize entryDetails to empty object', () => {
      entry = new SubEntry('2016-12-01', {});
      expect(entry.data.entryDetails).toEqual({});
    });

    it('should allow us to set entryDetails', () => {
      entry = new SubEntry('2016-12-01', {entryDetails: 'custom'});
      expect(entry.data.entryDetails).toEqual('custom');
    });

    it('should use the provided entryDate when it exists', () => {
      entry = new SubEntry('2016-12-01', {entryDate: '2017-01-01'});
      expect(entry.entryDate).toEqual('2017-01-01');
    });

    it('should use the default entryDate when otherwise not provided', () => {
      entry = new SubEntry('2016-12-01', {});
      expect(entry.entryDate).toEqual('2016-12-01');
    });

    it('should set exceptions if exceptions are present', () => {
      let data = {
        exceptions: [{
          exceptionDescription: 'SomeException',
          exceptionCode: {
            codeValue: 'EXCEPTION'
          },
          exceptionTypeCode: {
            codeValue: 'error'
          },
          actions: [] as any
        }]
      };

      let entry = new SubEntry('2016-12-01', data);
      expect(entry.hasException()).toEqual(true);
      expect(entry.hasAcknowledgedException()).toEqual(false);
    });

    it('should have acknowledged exceptions if all exceptions are acknowledged', () => {
      let data = {
        exceptions: [
          {
            exceptionDescription: 'SomeException',
            exceptionCode: {
              codeValue: 'EXCEPTION'
            },
            exceptionTypeCode: {
              codeValue: 'error'
            },
            acknowledgementIndicator: true
          },
          {
            exceptionDescription: 'SomeException',
            exceptionCode: {
              codeValue: 'EXCEPTION'
            },
            exceptionTypeCode: {
              codeValue: 'error'
            },
            acknowledgementIndicator: true
          }]
      };

      let entry = new SubEntry('2016-12-01', data);
      expect(entry.hasAcknowledgedException()).toEqual(true);
    });

    it('should not have acknowledged exceptions if only some exceptions are acknowledged', () => {
      let data = {
        exceptions: [
          {
            exceptionDescription: 'SomeException',
            exceptionCode: {
              codeValue: 'EXCEPTION'
            },
            exceptionTypeCode: {
              codeValue: 'error'
            },
            acknowledgementIndicator: true
          },
          {
            exceptionDescription: 'SomeException',
            exceptionCode: {
              codeValue: 'EXCEPTION'
            },
            exceptionTypeCode: {
              codeValue: 'error'
            },
            acknowledgementIndicator: false

          }]
      };

      let entry = new SubEntry('2016-12-01', data);
      expect(entry.hasAcknowledgedException()).toEqual(false);
    });

    it('should properly set the entryTypeCode', () => {
      entry = EntryTestHelper.createUnit();
      expect(entry.entryTypeCode).toEqual('unitEntry');
    });
  });

  describe('Comments', () => {

    it('should find comments that apply to filter', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time'
      );
      entry.addComment(comment);
      expect(entry.commentsApplyTo('Start Time')).toBe(true);
    });

    it('should not find comments that do not apply to filter', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time'
      );
      entry.addComment(comment);

      expect(entry.commentsApplyTo('End Time')).toBe(false);
    });

    it('should not count comments that have not yet been initialized with type', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        undefined,
        'Start Time'
      );
      entry.addComment(comment);

      expect(entry.commentsApplyTo('Start Time')).toBe(false);
    });

    it('should return true if comments exist', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time'
      );
      entry.addComment(comment);

      expect(entry.hasNewComments()).toBe(true);
    });

    it('should return false when there are no comments', () => {
      entry = new SubEntry('2016-12-01', {});
      expect(entry.hasNewComments()).toBe(false);
    });
  });

  describe('Validation', () => {

    it('should return false if entry is empty but has comments', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time'
      );
      spyOn(entry, 'isEmpty').and.returnValue(true);
      entry.addComment(comment);

      expect(entry.isValid()).toBe(false);
    });

    it('should return true if entry is empty and has no comments', () => {
      entry = new SubEntry('2016-12-01');
      spyOn(entry, 'isEmpty').and.returnValue(true);
      expect(entry.isValid()).toBe(true);
    });

    it('should return false if entry has a comment with a note exceeding 256 characters', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time',
        [new Note(
          _.repeat('a', 257),
          'G39M0630NN8K8X13',
          'Wang Hill'
        )]
      );
      spyOn(entry, 'isEmpty').and.returnValue(false);
      entry.addComment(comment);
      expect(entry.isValid()).toBe(false);
    });

    it('should return true if the entry has a comment and a note not exceeding 256 characters', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time',
        [new Note(
          'this is a note',
          'G39M0630NN8K8X13',
          'Wang Hill'
        )]
      );
      spyOn(entry, 'isEmpty').and.returnValue(false);
      entry.addComment(comment);

      expect(entry.isValid()).toBe(true);
    });

  });

  describe('Permissions', () => {
    it('should set canUpdate when timeEntry.modify operation is present', () => {
      entry = new SubEntry('2016-12-01', EntryTestHelper.getEntryData());
      expect(entry.data.canUpdate).toBe(true);
      expect(entry.data.canDelete).toBe(true);
    });

    it('should set canDelete when timeEntry.delete operation is present', () => {
      entry = new SubEntry('2016-12-01', EntryTestHelper.getEntryData());
      expect(entry.data.canUpdate).toBe(true);
      expect(entry.data.canDelete).toBe(true);
    });

    it('should set canUpdate when change operationID is present', () => {
      let entryData = EntryTestHelper.getEntryDataWithUpdatedActions();
      entry = new SubEntry('2016-12-01', entryData);
      expect(entry.data.canUpdate).toEqual(true);
    });

    it('should set canDelete when remove operationID is present', () => {
      let entryData = EntryTestHelper.getEntryDataWithUpdatedActions();
      entry = new SubEntry('2016-12-01', entryData);
      expect(entry.data.canDelete).toEqual(true);
    });

    it('should set canUpdate to false when actions are not present', () => {
      entry = new SubEntry('2016-12-01', EntryTestHelper.getReadOnlyEntryData());
      expect(entry.data.canUpdate).toBe(false);
      expect(entry.data.canDelete).toBe(false);
    });
  });

  describe('Inherited methods', () => {
    it('should delete', () => {
      entry = new SubEntry('2016-12-01');
      entry.delete();

      expect(entry.deleted).toEqual(true);
    });

    it('should undelete', () => {
      entry = new SubEntry('2016-12-01');
      entry.undelete();

      expect(entry.deleted).toEqual(false);
    });

    it('should getDate', () => {
      entry = new SubEntry('2016-12-01');
      entry.entryDate= '2015-07-24';
      expect(entry.getDate()).toEqual('2015-07-24');
    });

    it('should get entryNotesToDTO with newly added comment', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time'
      );
      entry.addComment(comment);
      var entryNotes = entry.entryNotesToDTO();

      expect(entryNotes[0]).toEqual(comment.toDTO()[0]);
    });

    it('should get entryNotesToDTO with newly added comment with note', () => {
      let note = new Note('foo');
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time',
        [note]
      );
      entry.addComment(comment);
      var entryNotes = entry.entryNotesToDTO();

      expect(entryNotes[0]).toEqual(comment.toDTO()[0]);
    });

    it('should get entryNotesToDTO with newly added comment with 2 note2', () => {
      let note1 = new Note('foo');
      let note2 = new Note('bar');
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time',
        [note1, note2]
      );
      entry.addComment(comment);
      var entryNotes = entry.entryNotesToDTO();

      expect(entryNotes[0]).toEqual(comment.toDTO()[0]);
      expect(entryNotes[1]).toEqual(comment.toDTO()[1]);
      expect(entryNotes.length).toEqual(2);
    });

    it('should get entryNotesToDTO with newly added comment and existing comment', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'End Time'
      );

      entry.comments.existing = [new Comment(
        'Mobile',
        'Start Time',
        [new Note(
          _.repeat('a', 257),
          'G39M0630NN8K8X13',
          'Wang Hill'
        )]
      )];
      entry.addComment(comment);
      var entryNotes = entry.entryNotesToDTO();

      expect(entryNotes.length).toEqual(1);
    });

    it('should get entryNotesToDTO with newly added comment and existing comment that has 2 notes', () => {
      let note1 = new Note('foo');
      let note2 = new Note('bar');
      entry = new SubEntry('2016-12-01', {});
      entry.comments.existing = [
        new Comment(
          'Running Late',
          'Start Time',
          [note1, note2]
        )
      ];
      entry.addComment(new Comment(
        'Mobile',
        'End Time'
      ));
      var entryNotes = entry.entryNotesToDTO();

      expect(entryNotes.length).toEqual(1);
    });

    it('should hasComment', () => {
      entry = new SubEntry('2016-12-01', {});
      entry.addComment(new Comment(
        'Mobile',
        'Start Time',
        [new Note(
          'This is a note',
          'G39M0630NN8K8X13',
          'Wang Hill'
        )]
      ));
      let entryNoComment = new SubEntry('2016-12-01', {});
      expect(entry.hasComment()).toEqual(true);
      expect(entryNoComment.hasComment()).toEqual(false);
    });

  });

  describe('toDTO', () => {
    it('should return entryID', () => {
      entry = new SubEntry('2016-12-01', {entryID: 'foo/123'});
      expect(entry.toDTO().entryID).toEqual('foo/123');
    });

    it('should return entryDate', () => {
      entry = new SubEntry('2016-12-01');
      expect(entry.toDTO().entryDate).toEqual('2016-12-01');
    });

    it('should return earningAllocations', () => {
      entry = new SubEntry('2016-12-01', {laborAllocations: [1]});
      expect(entry.toDTO().laborAllocations).toEqual([1]);
    });

    it('should return comments', () => {
      entry = new SubEntry('2016-12-01');
      expect(entry.toDTO().comments).toEqual([]);
    });

    it('should return a single comment', () => {
      entry = new SubEntry('2016-12-01');
      comment = new Comment(
        'Mobile',
        'Start Time'
      );
      entry.addComment(comment);

      expect(entry.toDTO().comments).toEqual(comment.toDTO());
    });

    it('should return a single comment with note', () => {
      let note = new Note('foo');
      entry = new SubEntry('2016-12-01');
      comment = new Comment(
        'Mobile',
        'Start Time',
        [note]
      );
      entry.addComment(comment);

      expect(entry.toDTO().comments).toEqual(comment.toDTO());
    });

    it('should return a single comments with multiple notes', () => {
      let note1 = new Note('foo');
      let note2 = new Note('bar');
      entry = new SubEntry('2016-12-01');
      comment = new Comment(
        'Mobile',
        'Start Time',
        [note1, note2]
      );
      entry.addComment(comment);

      expect(entry.toDTO().comments).toEqual([comment.toDTO()[0], comment.toDTO()[1]]);
    });

    it('should return mulitple comments', () => {
      entry = new SubEntry('2016-12-01');
      comment = new Comment(
        'Mobile',
        'Start Time'
      );
      entry.addComment(comment);

      comment = new Comment(
        'Mobile',
        'End Time'
      );
      entry.addComment(comment);

      expect(entry.toDTO().comments).toEqual(entry.comments.added[0].toDTO().concat(entry.comments.added[1].toDTO()));
    });

    it('should return only new added comments', () => {
      entry = new SubEntry('2016-12-01');
      comment = new Comment(
        'Mobile',
        'Start Time'
      );
      entry.comments.existing = [
        new Comment(
          'Running Late',
          'Start Time'
        )
      ];
      entry.addComment(comment);

      expect(entry.toDTO().comments).toEqual(comment.toDTO());
    });

    it('should attach any properties passed to base toDTO', () => {
      entry = new SubEntry('2016-12-01');
      let result = entry.toDTO({
        code: 'Sub Entry',
        type: 'fakeEntry'
      });
      expect(result.code).toEqual('Sub Entry');
      expect(result.type).toEqual('fakeEntry');
    });

    it('should not attach invalid comments to base toDTO', () => {
      entry = new SubEntry('2016-12-01');
      entry.addComment(new Comment('',
        undefined,
        [new Note('this is a note')],
        undefined,
        undefined,
        '1234',
        {
          appliesToCode: true,
          commentCode: true,
          textValue: false
      }));
      expect(entry.toDTO().comments.length).toEqual(0);
    });

    it('should attach an empty entryOverrideCode when there is no work rule', () => {
      entry = new SubEntry('2016-12-01');
      let result = entry.toDTO();
      expect(result.entryOverrideCode).toEqual([]);
    });

    it('should have paycode value is not undefined and is timepair', () => {
      let entryCodeOption = {
        entryCode: {
          codeValue: 'DBLTME',
          shortName: 'Doubletime'
        }
      };
      let entry = EntryTestHelper.createEasyTimePair('9:00 AM', '5:00 PM', entryCodeOption),
        dto = entry.toDTO();

      expect(dto.entryCode).toEqual('DBLTME');
    });

    it('should attach an entryOverrideCode when there is a work rule', () => {
      let entryOverrideCodes = {
        "entryOverrideCodes": [
          {
            "overrideCode": {
              "codeValue": "Admin Assistant",
              "shortName": "Admin Assistant"
            },
            "overrideTypeCode": {
              "codeValue": "work-rules",
              "shortName": "Work Rules"
            }
          },
          {
            "overrideCode": {
              "codeValue": "30min Meal Deduct",
              "shortName": "30min Meal Deduct"
            },
            "overrideTypeCode": {
              "codeValue": "cancel-deductions"
            }
          }
        ]
      };

      entry = new SubEntry('2016-12-01', _.assign(EntryTestHelper.getEntryData(), entryOverrideCodes));

      let result = entry.toDTO();
      expect(result.entryOverrideCode).toEqual([
        {
          "overrideCode": {
            "codeValue": "Admin Assistant"
          },
          "overrideTypeCode": {
            "codeValue": "work-rules"
          }
        }
      ]);
    });
  });

  describe('When I revert edits', () => {
    it('should revert work rules', () => {
      var entry = new SubEntry('2016-12-01', {});
      entry.data.workRule = [{} as any];

      entry.revertEdit();

      expect(entry.isEdited()).toEqual(false);
    });

    it('should revert transfer for all entries', function () {
      var entry = new SubEntry('2016-12-01', {});
      var original = entry.data.earningAllocations;

      entry.data.earningAllocations = ['Changed'];
      entry.revertEdit();

      expect(_.isEqual(entry.data.earningAllocations, 'changed')).toEqual(false);
      expect(_.isEqual(entry.data.earningAllocations, original)).toEqual(true);
      expect(entry.isEdited()).toEqual(false);
    });

    it('should revert added comments for all entries', function () {
      var entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time'
      );

      entry.comments.added.push(comment);

      entry.revertEdit();

      expect(entry.comments.added).toEqual([]);
      expect(entry.isEdited()).toEqual(false);
    });
  });

  describe('isEdited', () => {
    it('should report isEdited if hasUpdatedWorkRule', () => {
      entry = new SubEntry('2016-12-01', {});
      entry.originalWorkRule =[{
        overrideCode: { codeValue: 'User Selected', shortName: 'User Selected'},
        overrideTypeCode: { codeValue: 'work-rules', shortName: 'Work Rules' }
      }];
      entry.data.workRule = [{
        overrideCode: { codeValue: 'User Selected', shortName: 'User Selected'},
        overrideTypeCode: { codeValue: 'work-rules', shortName: 'Work Rules' }
      }];
      expect(entry.isEdited()).toEqual(false);
      entry.data.workRule = [{
        overrideCode: { codeValue: 'User Selected 2', shortName: 'User Selected'},
        overrideTypeCode: { codeValue: 'work-rules', shortName: 'Work Rules' }
      }];
      expect(entry.isEdited()).toEqual(true);
    });

    it('should report if the transfer is edited', () => {
      entry = new SubEntry('2016-12-01', {});
      expect(entry.isEdited()).toEqual(false);
      entry.data.earningAllocations = ['edited'];
      expect(entry.isEdited()).toEqual(true);
    });
  });

  describe('calls filterComments to return an array containing only comments with a type property defined', () => {
    it('should return an array with the valid comments', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        'Mobile',
        'Start Time'
      );
      entry.addComment(comment);
      let validAddedComments = entry.filterComments();

      expect(validAddedComments.length).toEqual(1);
    });

    it('should return an array with the ONLY valid comments', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        '',
        ''
      );
      entry.addComment(comment);
      let validAddedComments = entry.filterComments();

      expect(validAddedComments.length).toEqual(0);
    });

    it('should return an empty array when there are no valid comments', () => {
      entry = new SubEntry('2016-12-01', {});
      comment = new Comment(
        undefined,
        'Start Time'
      );
      entry.addComment(comment);
      var validAddedComments = entry.filterComments();

      expect(validAddedComments.length).toEqual(0);
    });

  });

  describe('When instantiating an Entry with work rule data', () => {

    beforeEach(() => {
      this.entryOverrideCodes = {
        "entryOverrideCodes": [
          {
            "overrideCode": {
              "codeValue": "Admin Assistant",
              "shortName": "Admin Assistant"
            },
            "overrideTypeCode": {
              "codeValue": "work-rules",
              "shortName": "Work Rules"
            }
          },
          {
            "overrideCode": {
              "codeValue": "30min Meal Deduct",
              "shortName": "30min Meal Deduct"
            },
            "overrideTypeCode": {
              "codeValue": "cancel-deductions"
            }
          }
        ]};
    });

    it('should attach work rule to entry when constructed', () => {
      entry = new SubEntry('2016-12-01', _.assign(EntryTestHelper.getEntryData(), this.entryOverrideCodes));
      expect(entry.data.workRule).toEqual([{
        overrideCode: { codeValue: 'Admin Assistant', shortName: 'Admin Assistant'},
        overrideTypeCode: { codeValue: 'work-rules', shortName: 'Work Rules' }
      }]);
    });

    it('should report being edited if new workRule differs from original', () => {
      entry = new SubEntry('2016-12-01', _.assign(EntryTestHelper.getEntryData(), this.entryOverrideCodes));
      expect(entry.isEdited()).toEqual(false);

      entry.data.workRule = [{
        overrideCode: { codeValue: 'User Selected', shortName: 'User Selected'},
        overrideTypeCode: { codeValue: 'work-rules', shortName: 'Work Rules' }
      }];

      expect(entry.isEdited()).toEqual(true);
    });
  });
});
