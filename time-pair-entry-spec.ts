import TimePairEntry from './time-pair-entry';
import Comment from '../comment/comment';
import Note from '../note/note';
import { EntryTestHelper } from '../../../../spec-helpers/spec-helpers';

describe('Timepair Entry', () => {

  let homeTransfer: any,
    otherTransfer: any;

  beforeEach(() => {
    homeTransfer = EntryTestHelper.createEasyEarningAllocations();
    otherTransfer = EntryTestHelper.createEasyEarningAllocations('a', 'foo');
  });

  describe('creation of TimePairEntry objects', () => {
    it('should be empty if initialized with empty timePeriod', () => {
      let entry = new TimePairEntry('2016-12-01', {});

      expect(entry.isEmpty()).toEqual(true);
    });

    it('should not be empty if initialized with a valid timePeriod', () => {

      let data = {
        startPeriod: {
          startDateTime: moment('2015-09-16T14:30:00-04:00', moment.ISO_8601, true)
        },
        endPeriod: {
          endDateTime: moment('2015-09-16T17:00:00-04:00', moment.ISO_8601, true)
        }
      };

      let entry = new TimePairEntry('2016-12-01', data);

      expect(entry.isEmpty()).toEqual(false);

    });

    it('should set the startTime if a default is specified', () => {
      let data = {
        defaultStartTime: 'PT8H'
      };
      let entry = new TimePairEntry('2016-12-01', data);
      expect(entry.toDTO().startDateTime).toEqual('2016-12-01T08:00:00-05:00');
    });

    it('should not the startTime if neither startTime or default is specified', () => {
      let entry = new TimePairEntry('2016-12-01',{});
      expect(entry.toDTO().startDateTime).toEqual(undefined);
    });

    it('should not set exception if exceptionType is not present', () => {
      let entry = new TimePairEntry('2016-12-01', {});
      expect(entry.hasException()).toEqual(false);
    });

    it('should set exception if exceptionType IS present', () => {
      let data = {
        startPeriod: {
          startDateTime: moment('2015-09-16T14:30:00-04:00', moment.ISO_8601, true)
        },
        endPeriod: {
          endDateTime: moment('2015-09-16T17:00:00-04:00', moment.ISO_8601, true)
        },
        exceptions: [{
          exceptionDescription: 'Unscheduled',
          exceptionCode: {
            codeValue: 'UNSCHEDULED'
          },
          exceptionTypeCode: {
            codeValue: 'error'
          },
          actions: [] as any
        }]
      };

      let entry = new TimePairEntry('2016-12-01', data);
      expect(entry.hasException()).toEqual(true);
      expect(entry.hasAcknowledgedException()).toEqual(false);
    });

    it('should return true if exceptions are acknowledged', () => {
      let data = {
        startPeriod: {
          startDateTime: moment('2015-09-16T14:30:00-04:00', moment.ISO_8601, true)
        },
        endPeriod: {
          endDateTime: moment('2015-09-16T17:00:00-04:00', moment.ISO_8601, true)
        },
        exceptions: [{
          exceptionDescription: 'Unscheduled',
          exceptionCode: {
            codeValue: 'UNSCHEDULED'
          },
          acknowledgementIndicator: true,
          exceptionTypeCode: {
            codeValue: 'information'
          }
        }]
      };

      let entry = new TimePairEntry('2016-12-01', data);
      expect(entry.hasAcknowledgedException()).toEqual(true);
    });

    it('should adjust the UTC Offset of new entries to match the timezone reported by the server', () => {

      let myUtcOffset = moment().utcOffset();
      let serverUtcOffset = myUtcOffset-100;

      let serverStartTimeIso = moment().utcOffset(serverUtcOffset).format();
      let serverEndTimeIso = moment().add(1, 'hour').utcOffset(serverUtcOffset).format();

      let data = {
        startPeriod: {
          startDateTime: moment(serverStartTimeIso, moment.ISO_8601, true).utcOffset(serverUtcOffset).format()
        },
        endPeriod: {
          endDateTime: moment(serverEndTimeIso, moment.ISO_8601, true).utcOffset(serverUtcOffset).format()
        }
      };

      let entry = new TimePairEntry('2016-12-01', data);

      expect(entry.startTime.utcOffset()).toEqual(serverUtcOffset);
      expect(entry.endTime.utcOffset()).toEqual(serverUtcOffset);
    });
  });

  describe('Comments', () => {
    it('should have comment indicator set to false by default', () => {
      let timePair = EntryTestHelper.createTimePair();
      expect(timePair.hasComment()).toBe(false);
    });

    it('should have comment indicator set be able to set the comment indicator', () => {
      let timePair = EntryTestHelper.createTimePair({comments: true});
      expect(timePair.hasComment()).toBe(true);
    });

    it('should set comment indicator to true', () => {
      let timePair = EntryTestHelper.createTimePair();
      timePair.data.commentIndicator = true;
      expect(timePair.hasComment()).toBe(true);
    });

    it('should add a comment to the entry', () => {
      let timePair = EntryTestHelper.createTimePair();
      let commentToAdd = new Comment('Running Late', 'Start Time', [new Note('note1')],'id','today');
      timePair.addComment(commentToAdd);
      expect(timePair.comments.added.length).toEqual(1);
    });

    it('should set the work rule if present on the start period', () => {
      let data = {
        startPeriod: {
          startDateTime: moment('2015-09-16T14:30:00-04:00', moment.ISO_8601, true),
          entryOverrideCodes: [
            {
              overrideCode: {
                codeValue: 'Guarantee',
                shortName: 'Guarantee'
              },
              overrideTypeCode: {
                codeValue: 'work-rules'
              }
            }
          ]
        },
        endPeriod: {
          endDateTime: moment('2015-09-16T17:00:00-04:00', moment.ISO_8601, true)
        }
      };

      let entry = new TimePairEntry('2016-12-01', data);
      expect(entry.data.workRule[0].overrideCode.codeValue).toEqual('Guarantee');
    });
  });

  it('should return the date using the accessor', () => {
    let timePair = EntryTestHelper.createTimePair({entryDate: '2015-02-17'});
    expect(timePair.getDate()).toEqual('2015-02-17');
  });

  it('should add home transfer to pairs with no earning allocations', () => {
    let timePair = EntryTestHelper.createTimePair({earningAllocations: []});
    expect(_.isEqual(timePair.data.earningAllocations, homeTransfer)).toBeTruthy();
  });

  it('should add entryDetails object to pairs if unset', () => {
    let timePair = EntryTestHelper.createTimePair({entryDetails: {}});
    expect(timePair.data.entryDetails).toEqual({});
  });

  it('should add entryDetails to pairs with existing data', () => {
    let timePair = EntryTestHelper.createTimePair({entryDetails: {exceptions: 'called'}});
    expect(timePair.data.entryDetails).toEqual({exceptions: 'called'});
  });

  it('should revert edited times', () => {
    let timePair = EntryTestHelper.createEasyTimePair('7:00 AM', '10:00 AM');
    timePair.startTime.set('hour', 20).set('minute', 0);

    expect(timePair.startTime.format('LT').toLowerCase()).toEqual('8:00 pm');
    expect(timePair.isEdited()).toBe(true);
    timePair.revertEdit();
    expect(timePair.startTime.format('LT')).toEqual('7:00 AM');
    expect(timePair.isEdited()).toBe(false);
  });

  it('should revert edited times with a missing end punch', () => {
    let timePair = EntryTestHelper.createEasyTimePair('7:00 AM', null);
    timePair.endTime = moment();

    expect(timePair.isEdited()).toBe(true);
    timePair.revertEdit();
    expect(timePair.endTime).toBeNull();
    expect(timePair.isEdited()).toBe(false);
  });

  it('should revert edited times with a missing start punch', () => {
    let timePair = EntryTestHelper.createEasyTimePair(null, '7:00 AM');
    timePair.startTime = moment();

    expect(timePair.isEdited()).toBe(true);
    timePair.revertEdit();
    expect(timePair.startTime).toBeNull();
    expect(timePair.isEdited()).toBe(false);
  });


  describe('Valid Entry', () => {
    it('should be a valid entry when endingTime is empty', () => {
      let timePair = EntryTestHelper.createEasyTimePair('8:00 AM', null);

      expect(timePair.isValidEndTime()).not.toBeTruthy();
      expect(timePair.isValid()).toBeTruthy();
    });

    it('should be valid when both starDateTime and endDateTime are set', () => {
      let timePair = EntryTestHelper.createEasyTimePair('8:00 AM', '9:00 AM');
      expect(timePair.isValid()).toBeTruthy();
    });

    it('should be invalid if start time is missing, but end time is defined', () => {
      let timePair = EntryTestHelper.createEasyTimePair(null, '5:00 PM');

      expect(timePair.endTime.isValid()).toBeTruthy();
      expect(timePair.startTime).toBe(null);
      expect(timePair.isValid()).toBe(false);
    });

    it('should flag empty time pair as valid', () => {
      let entry = EntryTestHelper.createEasyTimePair();

      expect(entry.isEmpty()).toEqual(true);
      expect(entry.isValid()).toBe(true);
    });

    it('should flag empty time pair with comment as invalid', () => {
      let entry = EntryTestHelper.createEasyTimePair(null, null);

      expect(entry.isEmpty()).toEqual(true);
      entry.addComment(new Comment('Mobile', 'Start Time'));
      expect(entry.isValid()).toBe(false);
    });

    it('should be invalid entry if end time is empty with comments', () => {
      let timePair = EntryTestHelper.createEasyTimePair('9:00 AM', null);

      expect(timePair.endTime).toBe(null);
      expect(timePair.isEmpty()).toEqual(false);
      timePair.addComment(new Comment('Mobile', 'End Time'));
      expect(timePair.isValid()).toBe(false);
    });

    it('should be invalid entry if start time is empty with comments', () => {
      let pair = EntryTestHelper.createEasyTimePair(null, '5:00 PM');
      expect(pair.isValidStartTime()).toEqual(false);
      pair.addComment(new Comment('Mobile', 'Start Time'));

      expect(pair.isValid()).toBe(false);
    });

    it('should be invalid entry if notes are invalid', () => {
      let timePair = EntryTestHelper.createEasyTimePair('8:00 AM', '10:00 AM');
      timePair.addComment(new Comment('Mobile', 'Start Time'));
      timePair.comments.added[0].addNote();
      timePair.comments.added[0].notes[0].text = _.repeat('A', 1000);

      expect(timePair.isValid()).toBe(false);
    });

  });

  describe('When editing an entry', () => {
    it('should mark entry as edited if the endTime becomes empty', function() {
      let timePair = EntryTestHelper.createEasyTimePair('4:00 PM', '9:00 PM');
      timePair.startTime = null;

      expect(timePair.isEdited()).toBe(true);
    });

    it('should mark entry as edited if the startTime becomes empty', function() {
      let timePair = EntryTestHelper.createEasyTimePair('4:00 PM', '9:00 PM');
      timePair.endTime = null;

      expect(timePair.isEdited()).toBe(true);
    });

    it('should mark time pair as edited if start time changes', () => {
      let timePair = EntryTestHelper.createEasyTimePair('4:00 PM', null);
      timePair.startTime.set('hour', 20).set('minute', 0);

      expect(timePair.isEdited()).toBe(true);
    });

    it('should mark time pair as edited if transfer changes', () => {
      let timePair = EntryTestHelper.createTimePair();
      timePair.data.earningAllocations = otherTransfer;

      expect(timePair.isEdited()).toBe(true);
    });

    it('should mark time pair as edited if starting time was updated to its existing values', () => {
      let timePair = EntryTestHelper.createTimePair();
      // These match the default fixture time
      timePair.startTime = moment('8:00 PM', 'h:mm A', true);
      expect(timePair.isEdited()).toBe(true);

      timePair = EntryTestHelper.createTimePair();
      timePair.endTime = moment('4:00 AM', 'h:mm A', true);

      expect(timePair.isEdited()).toBe(true);

    });

    it('Should mark the time pair as edited if the end time was only updated', () => {
      let  timePair = EntryTestHelper.createTimePair('2:00 PM');
      timePair.endTime = moment('5:00 PM', 'h:mm A', true);

      expect(timePair.isEdited()).toBe(true);
    });

    it('should not mark time pair as edited if neither starting time nor ending time changed', () => {
      let timePair = EntryTestHelper.createTimePair();
      expect(timePair.isEdited()).toBe(false);
    });

    it('should mark time pair as edited if end time changes', () => {
      let timePair = EntryTestHelper.createEasyTimePair('9:00 AM', '5:00 PM');
      timePair.endTime = moment();

      expect(timePair.isEdited()).toBe(true);
    });

    it('should mark time pair as edited if both end time and start time change', () => {
      let timePair = EntryTestHelper.createEasyTimePair('9:00 AM', '5:00 PM');
      timePair.startTime = moment('4:00 PM', 'h:mm A', true);
      timePair.endTime = moment('8:00 PM', 'h:mm A', true);

      expect(timePair.isEdited()).toBe(true);
    });

  });

  describe('Time estimates', () => {
    it('should return estimateTime when both start/end times are present', () => {
      let timePair = EntryTestHelper.createEasyTimePair('9:00 AM', '5:00 PM');
      let expected = moment.duration({hours: 8});
      let actual = timePair.estimateTime();

      expect(actual).toEqual(expected);
    });

    it('should return a zero duration if only startTime is present', () => {
      let timePair = EntryTestHelper.createEasyTimePair('9:00 am');
      let expected = moment.duration({hours: 0});
      let actual = timePair.estimateTime();

      expect(actual).toEqual(expected);
    });
  });

  describe('isEmpty', () => {
    it('should report empty when value is blank', () => {
      let entry = EntryTestHelper.createEasyTimePair('9:00 AM', '5:00 PM');
      delete entry.startTime;
      delete entry.endTime;

      expect(entry.isEmpty()).toEqual(true);
    });
  });

  describe('DTO', () => {
    it('should have starting and ending time for timepairs with start and end', () => {
      let entry = EntryTestHelper.createEasyTimePair('9:00 AM', '5:00 PM'),
        dto = entry.toDTO();

      expect(dto.entryTypeCode).toEqual('timePairEntry');
      expect(dto.laborAllocations).toEqual([]);
      expect(dto.comments).toEqual([]);

      // The time backend throws away the TZ
      expect(dto.startDateTime).toMatch('2014-10-13T09:00:00');
      expect(dto.endDateTime).toMatch('2014-10-13T17:00:00');
      expect(dto.entryID).toBeDefined();
    });

    it('should not empty ending time that are not present in timepair', () => {
      let entry = EntryTestHelper.createEasyTimePair('9:00 AM', null);
      expect(entry.toDTO().endDateTime).not.toBeDefined();
    });

  });


});
