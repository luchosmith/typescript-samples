import { Entry, EntryData } from './entry';
import { TimeConstants } from '../../constants';
import { EntryToDTO } from '../../interfaces';
import DurationParser from '../../../shared/services/duration-parser';

export interface TimePairEntryData extends EntryData {
  startPeriod?: any;
  endPeriod?: any;
  defaultStartTime?: string;
}

export default class TimePairEntry extends Entry {
  type: string = 'timePairEntry';
  name: string = 'timePairEntry';
  entryTypeCode: string = 'timePairEntry';
  defaultStartTime: string;
  startTime: moment.Moment;
  endTime: moment.Moment;
  originalStartTime: moment.Moment | null;
  originalEndTime:   moment.Moment | null;

  constructor(entryDate: string, data: TimePairEntryData) {
    super(entryDate, data);
    this.data.workRule = this.setWorkRuleValue(_.get(data, 'startPeriod', {}));
    this.originalWorkRule = _.cloneDeep(this.data.workRule);
    this.defaultStartTime = data.defaultStartTime;
    let timePeriod = this.createTimePeriod(data.startPeriod, data.endPeriod);
    this.startTime = (timePeriod.startDateTime) ? timePeriod.startDateTime.locale('en-US') : null;
    this.endTime = (timePeriod.endDateTime) ? timePeriod.endDateTime.locale('en-US') : null;
    this.originalStartTime = this.startTime && this.startTime.clone();
    this.originalEndTime = this.endTime && this.endTime.clone();
  }

  isValid(): boolean {
    if(!this.commentsAreValid()) {
      return false;
    }

    const isValidStartMoment = this.isValidStartTime();
    const isValidEndMoment = this.isValidEndTime();

    const startValid = !isValidStartMoment || isValidStartMoment;
    const endValid = !isValidEndMoment || (isValidEndMoment && isValidStartMoment);

    return startValid && endValid;
  }

  isValidStartTime(): boolean {
    return (!this.isNullOrUndefined(this.startTime) && moment.isMoment(this.startTime) && this.startTime.isValid());
  }

  isValidEndTime(): boolean {
    return (!this.isNullOrUndefined(this.endTime) && moment.isMoment(this.endTime) && this.endTime.isValid());
  }

  isEmpty(): boolean {
    return this.isNullOrUndefined(this.startTime) && this.isNullOrUndefined(this.endTime);
  }

  estimateTime(): moment.Duration {
    if(this.isValidStartTime() && this.isValidEndTime()) {
      return moment.duration({seconds: Math.abs(this.endTime.unix() - this.startTime.unix())});
    } else {
      return moment.duration({hours: 0});
    }
  }

  revertEdit(): void {
    super.revertEdit();
    this.startTime = (this.originalStartTime) ? this.originalStartTime.clone() : null;
    this.endTime = (this.originalEndTime) ? this.originalEndTime.clone() : null;
  }

  isEdited(): boolean {
    const baseEdited = super.isEdited();
    const start = this.isTimeEdited(this.originalStartTime, this.startTime);
    const end = this.isTimeEdited(this.originalEndTime, this.endTime);

    return baseEdited || start || end;
  }

  isEstimateable() {
    if(this.deleted === true) {
      return false;
    }
    return true;
  }

  toDTO(): EntryToDTO {
    const timePairSpecific = {
      entryTypeCode: this.entryTypeCode,
      endDateTime: (this.endTime) ? this.endTime.format() : undefined,
      endTimeEntryOverrideCode: {},
      startDateTime: this.startTimeToDTO(),
      startTimeEntryOverrideCode: this.data.workRule || {}
    };

    return super.toDTO(timePairSpecific);
  }

  private startTimeToDTO() {
    if (this.startTime) {
      return this.startTime.format();
    }
    if (this.defaultStartTime) {
      let midnight = moment(this.entryDate);
      return midnight.add(DurationParser.parse(this.defaultStartTime)).format();
    }
    return undefined;
  }

  private isTimeEdited(originalTime: any, currentTime: any) {
    if(!moment.isMoment(originalTime) || !moment.isMoment(currentTime)) {
      return originalTime !== currentTime;
    }
    return !(currentTime).isSame(originalTime);
  }

  private isNullOrUndefined(value: any) {
    return (_.isUndefined(value) || _.isNull(value));
  }

  private setWorkRuleValue(startPeriod: any) {
    if (_.isUndefined(startPeriod.entryOverrideCodes)) {
      return [];
    }
    let workRule = _.find(startPeriod.entryOverrideCodes, _.matchesProperty('overrideTypeCode.codeValue', 'work-rules'));
    return workRule ? [workRule] : [];
  }

  private createTimePeriod(startPeriod: any, endPeriod: any) {
    return {
      startDateTime: this.tryCreateMoment(_.get(startPeriod, 'startDateTime') as string),
      endDateTime: this.tryCreateMoment(_.get(endPeriod, 'endDateTime') as string)
    };
  }

  private tryCreateMoment(date: string) {
    if(!this.isNullOrUndefined(date)) {
      return moment(date, moment.ISO_8601, true).utcOffset(date);
    }
  }

  private commentsAreValid(): boolean {
    let isValid = true;

    if(this.isNullOrUndefined(this.startTime) && this.commentsApplyTo(TimeConstants.AppliesTo.START)) {
      isValid = false;
    }

    if(this.isNullOrUndefined(this.endTime) && this.commentsApplyTo(TimeConstants.AppliesTo.END)) {
      isValid = false;
    }

    return isValid && super.hasValidNotes();
  }

}
