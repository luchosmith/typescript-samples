import Comment from '../comment/comment';
import { EntryStatus } from '../../enums';
import Exception from '../exception/exception';
import { WorkRule, EntryToDTO} from '../../interfaces';
import { Allocation, Value } from 'redbox/time/common';

export interface PayCode {
  codeValue: string;
  shortName: string;
}

export interface BaseToDto {
  entryID: string;
  laborAllocations: any[];
  comments: any;
  entryOverrideCode: {};
  entryDate?: string;
  entryCode?: string;
}

export interface ValidPattern {
  pattern?: RegExp;
  minValue?: string;
  maxValue?: string;
}

export interface EntryData {
  entryID?: string;
  entryDate?: string;
  earningAllocations?: any[];
  laborAllocations?: any[];
  entryCode?: Value;
  commentIndicator?: boolean;
  entryDetails?: any;
  detailStatus?: EntryStatus;
  transfer?: TransferHistory;
  isReadOnly?: boolean;
  canUpdate?: boolean;
  canDelete?: boolean;
  codeName?: string;
  type?: string;
  code?: string;
  entryTypeCode?: string;
  actions?: any[];
  comments?: any;
  added?: boolean;
  deleted?: boolean;
  isEdited?: boolean;
  workRule?: WorkRule[] | null;
  validPattern?: ValidPattern;
  exceptions?: any[];
}

interface TransferHistory { }

export abstract class Entry {
  entryID: string;
  entryDate: string;
  code: string;
  data: EntryData = {};
  entryTypeCode: string;
  payCode: Value;
  entryCode: PayCode;
  exceptions: Exception[] = null;


  added: boolean = false;
  deleted = false;

  timeDuration: string;
  comments: {
    added: Comment[],
    existing: Comment[]
  };

  originalValue: any;
  originalTransfer: Allocation;
  originalWorkRule: WorkRule[];

  entryNotes: any;
  changeCode?: string;

  abstract type: string;
  abstract name: string;
  abstract isEstimateable(): boolean;

  constructor(entryDate: string, data: EntryData = {}) {
    _.merge(this.data, data);
    this.entryID = data.entryID;
    this.entryDate = data.entryDate || entryDate;
    this.entryCode = data.entryCode;
    this.entryTypeCode = data.type || _.get(data, 'entryTypeCode.codeValue') as string;
    this.data.earningAllocations = _.get(data, 'laborAllocations', []) as Allocation[];
    this.data.workRule = this.getWorkRuleValue(data);
    this.originalWorkRule = _.cloneDeep(this.data.workRule);
    this.originalTransfer = this.data.earningAllocations;
    this.data.commentIndicator = data.comments ? true : false;
    this.exceptions = Exception.createExceptions(data);
    this.data.canUpdate = data.added || this.canUpdateEntry(data);
    this.data.canDelete = data.added || this.canDeleteEntry(data);
    this.data.isReadOnly = !(this.data.canUpdate || this.data.canDelete);
    this.data.codeName = _.get(data, 'entryCode.shortName', '');
    this.data.code = data.code;
    this.comments = {
      added: [],
      existing: []
    };

    if (data.comments) {
      this.comments.existing = Comment.createComments(data.comments, this.entryDate);
    }

    this.data.entryDetails = data.entryDetails || {};
  }

  isValid(): boolean {
    if (this.isEmpty() && (this.hasNewComments() || this.hasNewNotes())) {
      return false;
    } else if (!this.hasValidNotes()) {
      return false;
    }
    return true;
  }

  isEdited(): boolean {
    let transfersDiffer = !_.isEqual(this.originalTransfer, this.data.earningAllocations);
    let hasNewComments = (this.filterComments().length > 0);
    let hasNewNotes = (this.filterNotes().length > 0);
    let originalWR = _.get(this.originalWorkRule, '[0]overrideCode.codeValue', '');
    let entryWR = _.get(this.data.workRule, '[0]overrideCode.codeValue', '');
    let hasUpdatedWorkRule = !_.isEqual(originalWR, entryWR);

    return transfersDiffer || hasNewComments || hasNewNotes || hasUpdatedWorkRule;
  }

  revertEdit(): void {
    this.comments.added = [];
    this.data.earningAllocations = _.cloneDeep(this.originalTransfer);
    this.data.workRule = _.cloneDeep(this.originalWorkRule);
  }

  isEmpty(): boolean {
    return false;
  }

  toDTO(extraProps: any = {}): EntryToDTO {
    let baseProperties: BaseToDto = {
      entryID: this.entryID,
      entryDate: this.entryDate,
      laborAllocations: this.data.earningAllocations,
      comments: this.entryNotesToDTO(),
      entryOverrideCode: this.workRuleToDTO()
    };

    if (this.entryTypeCode !== 'timePairEntry') {
      baseProperties.entryDate = this.entryDate;
    }

    if (!_.isUndefined(this.entryCode) && _.get(this, 'entryCode.codeValue') !== 'TimePair' ) {
         baseProperties.entryCode = this.entryCode.codeValue;
       }

    return _.merge(baseProperties, extraProps);
  }
  workRuleToDTO(): any {
    if (_.get(this.data.workRule, '[0]overrideCode')) {
      return _.map(this.data.workRule, (wr) => {
        return {
          overrideCode: {
            codeValue: wr.overrideCode.codeValue
          },
          overrideTypeCode: {
            codeValue: wr.overrideTypeCode.codeValue
          }
        };
      });
    }
    return [];
  }

  entryNotesToDTO(): any[] {
    let entryNotesToReturn = [] as any[];
    let commentArraytoDTO = this.filterNotes();
    for (let comment of commentArraytoDTO) {
      let commentDTO = comment.toDTO();
      if (!_.isEmpty(commentDTO)) {
        entryNotesToReturn = entryNotesToReturn.concat(comment.toDTO());
      }
    }
    return entryNotesToReturn;
  }

  addComment(comment: Comment): void {
    this.comments.added.push(comment);
  }

  removeComment(index: number) {
    this.comments.added.splice(index, 1);
  }

  removeNewComments() {
    this.comments.added = [];
  }

  commentsApplyTo(appliesTo: string): boolean {
    return _.some(this.comments.added, function (comment) {
      return comment.type && comment.appliesTo === appliesTo;
    });
  }

  hasComment(): boolean {
    var newComments = _.some(this.comments.added, function (comment) {
      return comment.type !== undefined || comment.notes.length > 0;
    });
    return this.data.commentIndicator || newComments || false;
  }

  hasNewComments(): boolean {
    return this.filterComments().length > 0;
  }

  hasNewNotes(): boolean {
    return this.filterNotes().length > 0;
  }

  filterComments(): Comment[] {
    let comments = _.filter(this.comments.added, 'type');
    return this.filterEmptyComments(comments);
  }

  filterNotes(): Comment[] {
    let notes = _.filter(this.comments.added, 'notes');
    return this.filterEmptyComments(notes);
  }

  filterEmptyComments(comments: Comment[]): Comment[] {
    return _.filter(comments, (comment) => {
      return comment.notes.length || comment.type;
    });
  }

  getAddedComments(): Comment[] {
    let filterNotes = this.filterNotes();
    let filterComments = this.filterComments();
    return filterNotes.concat(filterComments);
  }

  hasValidNotes(): boolean {
    return _.every(this.comments.added, function (comment) {
      return _.isEmpty(comment.notes) || _.every(comment.notes, (note) => {
        return note.isValid();
      });
    });
  }

  delete(): void {
    this.deleted = true;
  }

  undelete(): void {
    this.deleted = false;
  }

  getDate(): string {
    return this.entryDate;
  }

  get laborAccount(): string {
    return _.get(this.data.earningAllocations[0], 'allocationCode.codeValue', 'Home Account');
  }

  hasNotesByOthers(loggedInAoid: string): boolean {
    return _.some(this.comments.existing, (comment: Comment) => {
      return _.some(comment.notes, (note) => {
        return note.authoredByOther(loggedInAoid);
      });
    });
  }

  hasException(): boolean {
    return !_.isEmpty(this.exceptions);
  }

  hasAcknowledgedException(): boolean {
    let allDetailsAcknowledged = _.all(this.exceptions, function(e) {
      return e.acknowledgement.isAcknowledged || false;
    });

    return allDetailsAcknowledged && this.exceptions.length > 0;
  }

  private getWorkRuleValue(entryData: any): WorkRule[] {
    let workRule = _.find(entryData.entryOverrideCodes, _.matchesProperty('overrideTypeCode.codeValue', 'work-rules'));
    return workRule ? [workRule] : [];
  }

  private canUpdateEntry(data: EntryData): boolean {
    let enterpriseETime = _.any(data.actions, _.matchesProperty('operationID', 'timeEntry.modify'));
    let tlm = _.any(data.actions, _.matchesProperty('operationID', 'change'));
    return enterpriseETime || tlm;
  }

  private canDeleteEntry(data: EntryData): boolean {
    let enterpriseETime = _.any(data.actions, _.matchesProperty('operationID', 'timeEntry.delete'));
    let tlm = _.any(data.actions, _.matchesProperty('operationID', 'remove'));
    return enterpriseETime || tlm;
  }

}
