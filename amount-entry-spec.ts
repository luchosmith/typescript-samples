import AmountEntry from './amount-entry';
import { EntryTestHelper } from '../../../../spec-helpers/spec-helpers';

describe('Amount Entry', function () {

  let homeTransfer: any,
    otherTransfer: any,
    Comment: any,
    entry: AmountEntry;

  beforeEach(() => {
    angular.mock.module('redbox.time.Time');
    angular.mock.module('redbox.time.timecard');
    angular.mock.module('test.time.helpers.entry');

    inject(($injector: angular.auto.IInjectorService) => {
      Comment = $injector.get<any>('redbox.time.services.CommentModel');
    });

    homeTransfer = EntryTestHelper.createEasyEarningAllocations();
    otherTransfer = EntryTestHelper.createEasyEarningAllocations('a', 'foo');

  });

  describe('Newly constructed AmountEntry', () => {
    it('should have data.type set to totalAmount', () => {
      entry = new AmountEntry('2016-12-01', {});
      expect(entry.data.type).toEqual('totalAmount');
    });

    describe('From server response with existing amountValue set', () => {
      it('should set data.amountValue=3000 when amountValue exists and set to 3000', function () {
        entry = new AmountEntry('2016-12-01', { amount: { amountValue: '3000' } });
        expect(entry.data.value).toEqual(3000);
      });

      it('should not be marked as edited', () => {
        entry = new AmountEntry('2016-12-01', { amount: { amountValue: '3000' } });
        expect(entry.isEdited()).toEqual(false);
      });

      it('should not be marked as empty', () => {
        entry = new AmountEntry('2016-12-01', { amount: { amountValue: '3000' } });
        expect(entry.isEmpty()).toEqual(false);
      });
    });

    describe('From newly created instances on the timecard', () => {
      it('should not have value defined', () => {
        entry = new AmountEntry('2016-12-01', {});
        expect(entry.data.value).toBeUndefined();
      });

      it('should not have currencyCode set', () => {
        entry = new AmountEntry('2016-12-01', {});
        expect(entry.data.currencyCode).toBeUndefined();
      });
    });
  });


  it('should mark empty as valid', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: undefined }));
    expect(entry.isValid()).toBe(true);

    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: '' }));
    expect(entry.isValid()).toBe(true);
  });

  it('should flag valid decimal as valid', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: 4 }));
    expect(entry.isValid()).toBe(true);

    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: 4.30 }));
    expect(entry.isValid()).toBe(true);

    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: '4.30' }));
    expect(entry.isValid()).toBe(true);
  });

  it('should parse strings as valid numbers', () => {

    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: 'test' }));
    expect(entry.data.value).toBeUndefined();
    expect(entry.isValid()).toBe(true);

    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: '123Test' }));
    expect(entry.data.value).toEqual(123);
    expect(entry.isValid()).toEqual(true);
  });

  it('should flag empty amount with comments as invalid', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: '' }));
    entry.addComment(new Comment('Mobile', 'Start Time'));
    expect(entry.isValid()).toBe(false);
  });

  it('should have comment indicator set to false by default', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData());
    expect(entry.hasComment()).toBe(false);
  });

  it('should return the date with getDate()', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({}));
    expect(entry.getDate()).toEqual('2016-12-01');
  });


  it('should set comment indicator to true', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData());

    entry.data.commentIndicator = true;
    expect(entry.hasComment()).toBe(true);
  });

  it('should mark amount model as changed if amount changed', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: 4 }));

    entry.data.value = 10;
    expect(entry.isEdited()).toBe(true);
  });

  it('should add earning allocations to amountEntry if unset', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ earningAllocations: [] }));
    expect(_.isEqual(entry.data.earningAllocations, homeTransfer)).toBeTruthy();
  });

  it('should mark amount model as changed if earningAllocations change', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ earningAllocations: [] }));

    entry.data.earningAllocations = otherTransfer;
    expect(entry.isEdited()).toBe(true);
  });

  it('should add entryDetails to amountEntry if unset', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData());

    expect(entry.data.entryDetails).toEqual({});
  });

  it('should add entryDetails to amountEntry with existing data', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ entryDetails: { exceptions: 'called' } }));

    expect(entry.data.entryDetails).toEqual({ exceptions: 'called' });
  });

  it('should not mark amount model as changed if amount did not change', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData());
    expect(entry.isEdited()).toBe(false);
  });

  it('should revert amount', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData({ value: 4 }));
    entry.data.value = 10;
    entry.data.earningAllocations = ['changed'];

    expect(entry.isEdited()).toBe(true);

    entry.revertEdit();
    expect(entry.isEdited()).toBe(false);
    expect(entry.data.value).toEqual(4);
    expect(_.isEqual(entry.data.earningAllocations, homeTransfer)).toBeTruthy();
  });

  it('should add a comment to the entry', () => {
    entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData());
    let commentToAdd = new Comment('Running Late', 'Start Time', ['note1'], { itemID: 'id', entryDateTime: 'today' });

    entry.addComment(commentToAdd);
    expect(entry.comments.added.length).toEqual(1);
  });

  describe('isEmpty', function () {
    it('should report empty when value is blank', () => {
      entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData());
      entry.data.value = undefined;
      expect(entry.isEmpty()).toEqual(true);
    });

    it('should report non-empty when value is defined', () => {
      entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData());
      entry.data.value = 1;
      expect(entry.isEmpty()).toEqual(false);
    });
  });

  describe('Exceptions', function () {
    it('should always return false for hasException()', () => {
      entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData());
      expect(entry.hasException()).toEqual(false);
    });
  });

  describe('DTO', function () {
    it('should send required fields', () => {
      entry = new AmountEntry('2016-12-01', EntryTestHelper.createAmountData());
      let dto = entry.toDTO();

      expect(dto.entryTypeCode).toEqual('amountEntry');
      expect(dto.laborAllocations).toEqual([]);
      expect(dto.comments).toEqual([]);
      expect(dto.amountValue).toEqual(10.99);
    });
  });
});
