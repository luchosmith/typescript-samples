import { Entry, EntryData, PayCode, ValidPattern } from './entry';
import { EntryToDTO } from '../../interfaces';

export interface AmountEntryData extends EntryData {
  _value?: number | string;
  value?: number | string;
  type?: string;
  currencyCode?: string;
  amount?: {
    amountValue: string
  };
  entryCode?: PayCode;
  validPattern?: ValidPattern;
}

export default class AmountEntry extends Entry {
  type: string = 'amountEntry';
  name: string = 'amountEntry';
  entryTypeCode: string = 'amountEntry';
  originalValue: number;
  data: AmountEntryData;
  payCode: PayCode;
  validPattern: RegExp;
  maxValue: string;
  minValue: string;

  constructor(entryDate: string, data: AmountEntryData = {}) {
    super(entryDate, data);
    Object.defineProperty(this.data, 'value', {
      set: (v: any) => {
        data._value = parseFloat(v) || undefined;
      },
      get: () => {
        return data._value;
      }
    });
    this.data.type = 'totalAmount';
    this.data.value = _.get(data, 'amount.amountValue') as number;
    this.originalValue = this.data.value;
    this.payCode = data.entryCode;
    this.validPattern = _.get(data, 'validPattern.pattern') as RegExp;
    this.maxValue = _.get(data, 'validPattern.maxValue') as string;
    this.minValue = _.get(data, 'validPattern.minValue') as string;
  }

  isValid(): boolean {
    if (!super.isValid.call(this)) {
      return false;
    }
    if (this.isEmpty()) {
      return true;
    }

    return this.validPattern.test(this.data.value.toString());
  }

  isEmpty(): boolean {
    return _.isUndefined(this.data.value) || _.isNull(this.data.value) || this.data.value.toString() === '';
  }

  isEdited(): boolean {
    let baseEdited = super.isEdited();
    return baseEdited || this.data.value !== this.originalValue;
  }

  revertEdit(): void {
    super.revertEdit();
    this.data.value = this.originalValue;
  }

  hasException(): boolean {
    return false;
  }

  isEstimateable() {
    return false;
  }

  toDTO(): EntryToDTO {
    let amountSpecific = {
      entryCode: this.payCode.codeValue,
      entryTypeCode: this.entryTypeCode,
      amountValue: parseFloat(this.data.value.toString())
    };

    return super.toDTO(amountSpecific);
  }
}
